﻿using MinhaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MinhaAPI.Controllers
{
    public class EmpregadosController : ApiController
    {
        private List<Empregado> Funcionarios;

        public EmpregadosController()
        {
            Funcionarios = new List<Empregado> {
               new Empregado { Id = 1 , Nome = "Manuel", Apelido = "Luz"},
               new Empregado { Id = 2 , Nome = "Joana", Apelido = "Luz"},
               new Empregado { Id = 3 , Nome = "Carlos", Apelido = "Luz"},
        };
            
        }
        // GET: api/Empregados
        public List<Empregado> Get()
        {
            return Funcionarios;
        }
        //GET: api/Empregados/GetNomes
        [Route("api/Empregados/GetNomes")]
        public List<string> GetNomes()
        {
            List<string> output = new List<string>();
            foreach (var e in Funcionarios)
            {
                output.Add(e.Nome);
            }
            return output;
        }

        // GET: api/Empregados/5
        public Empregado Get(int id)
        {

            return Funcionarios.FirstOrDefault(x => x.Id == id);
        }

        // POST: api/Empregados
        public void Post([FromBody]Empregado valor)
        {
            Funcionarios.Add(valor);
        }

       

        // DELETE: api/Empregados/5
        public void Delete(int id)
        {
        }
    }
}
