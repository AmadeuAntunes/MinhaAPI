﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinhaAPI.Models
{
    public class Biblioteca
    {
        private static List<Livro> livros;
        public static List<Livro> Livros
        {
            get
            {
                if(livros == null)
                     GerarLivros();
                return livros;
            }
            set
            {
                livros = value;
            }
        }

        private static void GerarLivros()
        {
            livros = new List<Livro>();
            Livros.Add(new Livro {Id =1, Titulo = "O Cet 30", Autor = "Amadeu"} );
            Livros.Add(new Livro { Id = 2, Titulo = "O 30 e o CET", Autor = "Ricardo" });
            Livros.Add(new Livro { Id = 3, Titulo = "O Cinel", Autor = "Não sei" });

        }
    }
}